/**
*  Markers used by each users.
*/
export const PLAYER_MARKERS  = {
  PLAYER_X : 'X',
  PLAYER_O : 'O'
}


/**
* List of possible actions in the game area.
*/
export enum  GAME_AREA_ACTIONS {
  INIT_AND_WAIT,
  PLAY,
  RESTORE_AND_PLAY,
  RESET,
  CLOSE,
  WIN
}

export const EMPTY_GAME_AREA = [
  ['', '', ''],
  ['', '', ''],
  ['', '', '']
];

/**
 * The winner should place his markers in one of these mark configurations
 */
export const WINNING_POSSIBILITIES = [
  [[0, 0], [0, 1], [0, 2]],
  [[1, 0], [1, 1], [1, 2]],
  [[2, 0], [2, 1], [2, 2]],
  [[0, 0], [1, 0], [2, 0]],
  [[0, 1], [1, 1], [2, 1]],
  [[0, 2], [1, 2], [2, 2]],
  [[0, 0], [1, 1], [2, 2]],
  [[0, 2], [1, 1], [2, 0]]
];

export const GAME_LOCAL_STORAGE_NAME = 'morpion_parties';
