import { JSPlayer } from "./JSPlayer";

/**
 * Explains how a player should be defined.
 */
export interface IPlayer {

  get id():number;
  set id(value: number);

  get name():string;
  set name(value: string);

  get marker():string;
  set marker(value: string);

  get score():number;
  set score(value: number);

  /**
   * create an instance of a player from a simple Javascript object
   */
  fromOject(object : JSPlayer): IPlayer;
}
