import { IPlayer } from "./IPlayer";
import { PLAYER_MARKERS } from "../../const";
import { JSPlayer } from "./JSPlayer";


export class Player implements IPlayer{

  id = 1;
  name = '';
  marker = '';
  score = 0;

  constructor();
  constructor(id: number | null, name: string | null, marker: string | null);
  constructor(player: JSPlayer | null);
  constructor(idOrPlayer?: number | JSPlayer | null, name?: string | null, marker?: string | null){
    if(typeof idOrPlayer === 'number' || idOrPlayer === null){
      this.id = idOrPlayer as number ?? Date.now();
      this.name = name??'';
      this.marker = marker??'';
    }else{
      this.fromOject(idOrPlayer as JSPlayer);
    }
  }

  fromOject(object : JSPlayer) : Player{
    this.id = object.id;
    this.name = object.name;
    this.marker = object.marker;
    this.score = object.score;
    return this;
  }
}
