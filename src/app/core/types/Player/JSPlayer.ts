export class JSPlayer extends Object{
  id: number = 0;
  name: string = '';
  marker: string = '';
  score: number = 0;
}
