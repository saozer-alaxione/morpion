import { EMPTY_GAME_AREA } from "../../const";
import { JSPlayer } from "../Player/JSPlayer";

export class JSParty extends Object{
  id: number = 0;
  players: object[] = [];
  board_setup: string[][] = EMPTY_GAME_AREA.map(row => row.slice());
  lastPlayer: JSPlayer | null = new JSPlayer();
  winner: JSPlayer | null = new JSPlayer();
}
