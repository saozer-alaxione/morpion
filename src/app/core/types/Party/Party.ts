import { IParty } from "./IParty";
import { Player } from "../Player/Player";
import { EMPTY_GAME_AREA } from "../../const";
import { JSPlayer } from "../Player/JSPlayer";
import { JSParty } from "./JSParty";

export class Party implements IParty{

  id : number;
  players: Player[] = [];
  board_setup: string[][] = EMPTY_GAME_AREA.map(row => row.slice());
  lastPlayer: Player | null = null;
  winner: Player | null = null;

  constructor(object : JSParty | null = null) {
    if(!object){
      this.id = Date.now();
    }else{
      this.id = object.id;
      this.fromOject(object);
    }
  }

  addPlayer(player:Player):boolean{
    if(!this.playerExists(player)){
      this.players.push(player);
      this.winner = null;
      return true;
    }
    return false;
  }

  playerExists(player: Player) : boolean{
    let player_found = false;
    this.players.forEach((pl) => {
      if(player.id == pl.id || (player.name == pl.name || player.marker == pl.marker)){
        player_found = true;
      }
    });
    return player_found;
  }


  hasPlayers(min: number = 0):boolean{
    return this.players.length >= min;
  }



  hasWinner(){
    return this.winner !== null;
  }


  getPlayerById(id: number):Player | null{
    let player = null;
    this.players.forEach((pl) => {
      if(pl.id == id){
        player = pl;
      }
    });
    return player;
  }
  getPlayerByName(playerName: string):Player | null{
    let player = null;
    this.players.forEach((pl) => {
      if(pl.name == playerName){
        player = pl;
      }
    });
    return player;
  }

  getPlayerByMarker(marker: string):Player | null{
    let player = null;
    this.players.forEach((pl) => {
      if(pl.marker == marker){
        player = pl;
      }
    });
    return player;
  }

  updatePlayer(player : Player){
    this.players.forEach((pl) => {
      if(pl.id == player.id){
        pl = player;
      }
    });
  }

  updateBoard(board: Array<Array<string>>){
    this.board_setup = board
  }
  clear(): void {
    this.board_setup = EMPTY_GAME_AREA.map(row => row.slice());
    this.players = [];
    this.winner = null;
  }
  restart(): void {
    this.board_setup = EMPTY_GAME_AREA.map(row => row.slice());
    this.winner = null;
    /* this.players.forEach((player) => {
      player.score = 0;
    }); */
  }

  fromOject(object : JSParty): Party{
    this.id = object.id;
    this.players = object.players.map((player)=>{
      return new Player(player as JSPlayer);
    });
    this.id = object.id;
    this.board_setup = object.board_setup;
    this.lastPlayer = object.lastPlayer != null ? new Player(object.lastPlayer as JSPlayer) : null;
    this.winner = object.winner != null ? new Player(object.winner as JSPlayer) : null;
    return this;
  }
}
