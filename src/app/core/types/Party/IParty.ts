import { IPlayer } from "../Player/IPlayer";
import { JSParty } from "./JSParty";

/**
 * Explains how a party is stored in the storage.
 */
export interface IParty {
  get id() : number;
  set id(value);
  get players(): Array<IPlayer>;
  set players(value: IPlayer);

  /**
   * The last player in action.
   */
  get lastPlayer(): IPlayer | null;
  set lastPlayer(value: IPlayer | null);

  /**
   * The winner of this party.
   */
  get winner(): IPlayer | null;
  set winner(value: IPlayer | null);

  get board_setup(): Array<Array<string>>;
  set board_setup(value: Array<Array<string>>);

  /**
   * Add a new player in the party
   *
   * @param player The Player instance to add in the party.
   */
  addPlayer(player: IPlayer):boolean;

  /**
   * Check if the player is already in the party.
   *
   * @param player player to find
   */
  playerExists(player: IPlayer):boolean;

  /**
   * Check if the party has $min players.
   *
   * @param min Minimun number to find in order to validate this party.
   * @returns true | false
   */
  hasPlayers(min: number):boolean;

  /**
   * Check if the party has a winner.
   *
   * @returns true | false
   */
  hasWinner():boolean;

  /**
   * Try to get a player by providing an ID
   *
   * @param id ID to find in the party players array.
   * @returns player | null
   */
  getPlayerById(id: number):IPlayer | null;

  /**
   * Try to get a player by providing a name
   *
   * @param playerName name to find in the party players array.
   * @returns player | null
   */
  getPlayerByName(playerName: string):IPlayer | null;

  /**
   * Try to get a player by providing a name
   *
   * @param marker marker to find in the party players array.
   * @returns player | null
   */
  getPlayerByMarker(marker: string):IPlayer | null;

  /**
   * Update informations of one Player.
   *
   * @param board the new board setup.
   */
  updatePlayer(player: IPlayer):void;

  /**
   * Replace the current board with a new one.
   *
   * @param board the new board setup.
   */
  updateBoard(board: Array<Array<string>>):void;

  /**
   * Clear the board and delete entire users.
   */
  clear():void;

  /**
   * Keep users but reset the area and the scores
   */
  restart():void;

  /**
   * create an instance of a Party from a simple Javascript object
   */
  fromOject(object : JSParty): IParty;
}
