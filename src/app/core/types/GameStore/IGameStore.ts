import { IParty } from "../Party/IParty";

export interface IGameStore{

  /**
   * In which variable to store the game setup
   */
  get storageName(): string;
  set storageName(list: string);

  /**
   * List of stored parties on this browser
   */
  get parties(): Array<IParty>;
  set parties(list: Array<IParty>);

  /**
   * The last played party.
   */
  get lastParty() : IParty | null;
  set lastParty(party : IParty | null);

  /**
   *
   * @param id The identifier of the party that you would like to retreive.
   * @returns Party | null
   */
  findParty(id: number): IParty | null;

  /**
   *
   * @param party The partie instance that you would like to send on store.
   */
  saveParty(party: IParty):void;

  /**
   *
   * @param party The partie instance that you would like to update.
   * @returns The old and the new instance. null if the Party doesn't exists.
  */
 updateParty(party: IParty): void;

 /**
  *
  * @param party The partie instance that you would like to remove from store.
  * @returns true | false.
  */
 deleteParty(party: IParty):void;

 /**
  * Persist this store in the local storage.
  */
 persistDatas():void;

 /**
  * reset the browser storage.
  */
 destroy():void;

}
