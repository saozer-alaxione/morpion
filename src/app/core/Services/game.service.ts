import { Injectable } from '@angular/core';
import { IGameStore } from '../types/GameStore/IGameStore';
import { Party } from '../types/Party/Party';
import { GAME_LOCAL_STORAGE_NAME } from '../const';
import { IParty } from '../types/Party/IParty';

@Injectable({
  providedIn: 'root'
})
export class GameService implements IGameStore{
  parties: Array<Party> = [];
  lastParty: Party | null = null;
  storageName: string = GAME_LOCAL_STORAGE_NAME;

  constructor() {
    let storageDatas = JSON.parse(localStorage.getItem(this.storageName) || '{"parties": [], "lastParty": null}');
    this.parties = storageDatas.parties;
    this.lastParty = storageDatas.lastParty;
  }


  findParty(id: number): IParty | null {
    let result = null;
    this.parties.forEach((party: Party)=>{
      if(party.id == id){
        result = party;
      }
    });
    return result;
  }
  saveParty(party: IParty): void {
    if(!this.findParty(party.id)){
      this.parties.push(party);
      this.lastParty = party;
      this.persistDatas();
    }else{
      this.updateParty(party);
    }
  }
  updateParty(newParty: IParty): void {
    this.parties.forEach((party: Party)=>{
      if(party.id == newParty.id){
        party.players = newParty.players;
        party.lastPlayer = newParty.lastPlayer;
        party.board_setup = newParty.board_setup;
        this.lastParty = newParty;
        this.persistDatas();
      }
    });
  }
  deleteParty(deletion: IParty): void {
    let new_parties = this.parties.filter((party: Party)=>deletion.id!=party.id);
    this.parties = new_parties;
    this.persistDatas();
  }

  persistDatas(): void {
    localStorage.setItem(this.storageName, JSON.stringify({parties: this.parties, lastParty: this.lastParty}));
  }
  destroy():void{
    localStorage.removeItem(this.storageName);
  }

}
