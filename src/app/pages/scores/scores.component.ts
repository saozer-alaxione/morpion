import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Party } from '../../core/types/Party/Party';
import { Player } from '../../core/types/Player/Player';
import { PLAYER_MARKERS } from '../../core/const';
import { GameService } from '../../core/Services/game.service';

@Component({
  selector: 'app-scores',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.scss']
})
export class ScoresComponent {
  player_1 : Player = new Player(1, '', PLAYER_MARKERS.PLAYER_X);
  player_2 : Player = new Player(2, '', PLAYER_MARKERS.PLAYER_O);
  constructor(private gameService: GameService) {
    this.fetchPlayers();
  }
  ngOnInit(): void {
    this.fetchPlayers();
  }

  fetchPlayers(){
    let party = new Party(this.gameService.lastParty);
    if(party!.hasPlayers(2)){
      this.player_1 = party!.getPlayerById(1)!;
      this.player_2 = party!.getPlayerById(2)!;
    }else{
      this.player_1 = new Player(1, '', PLAYER_MARKERS.PLAYER_X);
      this.player_2 = new Player(2, '', PLAYER_MARKERS.PLAYER_O);
    }
  }
}
