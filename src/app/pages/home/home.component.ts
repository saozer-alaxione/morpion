import { Component, OnInit } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';
import { RouterLink } from '@angular/router';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { EMPTY_GAME_AREA, GAME_AREA_ACTIONS, PLAYER_MARKERS } from '../../core/const';
import { Party } from '../../core/types/Party/Party';
import { Player } from '../../core/types/Player/Player';
import { BoardComponent } from '../../components/board/board.component';
import { GameService } from '../../core/Services/game.service';

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  imports: [NgFor, NgIf, RouterLink, ReactiveFormsModule, BoardComponent],
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  party = new Party();
  player_1 : Player = new Player(1, '', PLAYER_MARKERS.PLAYER_X);
  player_2 : Player = new Player(1, '', PLAYER_MARKERS.PLAYER_O);
  player_x = new FormControl('');
  player_o = new FormControl('');
  error = '';
  has_winner = false;
  area_command = GAME_AREA_ACTIONS.INIT_AND_WAIT;
  restore = EMPTY_GAME_AREA.map(row => row.slice());

  constructor(private gameService: GameService) {
    this.party = new Party(gameService.lastParty);
    if(!this.party.hasPlayers(2) ){
      this.player_1 = new Player(1, this.player_x.value, PLAYER_MARKERS.PLAYER_X);
      this.player_2 = new Player(2, this.player_o.value, PLAYER_MARKERS.PLAYER_O);
      this.party.players = [
        this.player_1, this.player_2
      ];
      gameService.saveParty(this.party);
      this.player_x.setValue(this.player_1.name?? '');
      this.player_o.setValue(this.player_2.name?? '');
    }else{
      this.player_1 = this.party.getPlayerById(1)!;
      this.player_2 = this.party.getPlayerById(2)!;
      this.player_x.setValue(this.player_1.name?? '');
      this.player_o.setValue(this.player_2.name?? '');
      if(this.checkPlayerNames()){
        this.restore = this.party.board_setup;
        this.area_command = GAME_AREA_ACTIONS.RESTORE_AND_PLAY;
      }
    }
  }

  ngOnInit(): void {
  }


  cleanArea(): void {
    this.party.restart();
    this.gameService.updateParty(this.party!);
    this.area_command = GAME_AREA_ACTIONS.RESET;
    this.has_winner = false;
  }

  resetGame(): void {
    this.player_x.setValue('');
    this.player_o.setValue('');
    this.party!.clear();
    this.gameService.updateParty(this.party!);
    this.area_command = GAME_AREA_ACTIONS.INIT_AND_WAIT;
    this.has_winner = false;
  }

  setPlayers()
  {
    this.player_1.name = this.player_x.value!
    this.player_2.name = this.player_o.value!
    this.party.players= [
      this.player_1,
      this.player_2
    ];
    if(this.player_x.value !== '' && this.player_o.value! !== ''){
      this.area_command = GAME_AREA_ACTIONS.RESET;
    }else{
      this.area_command = GAME_AREA_ACTIONS.INIT_AND_WAIT;
    }
    this.gameService.updateParty(this.party);
  }

  checkPlayerNames(): boolean
  {
    if(this.player_x.value !== '' && this.player_o.value! !== ''){
      return true
    }
    return false;
  }
  handle_board_action($event: { type: GAME_AREA_ACTIONS, marker: string, board: string[][] }){
    this.area_command = $event.type;
    if($event.type == GAME_AREA_ACTIONS.WIN){
      this.party.winner = this.party.getPlayerByMarker($event.marker);
      this.party.winner!.score++;
      this.party.updatePlayer(this.party.winner!);
      this.has_winner = true;
    }
    this.party instanceof Party ? this.party.updateBoard($event.board.map(row => row.slice())) : null;
    this.gameService.saveParty(this.party as Party);
  }

  showWinner(){
    var winnerName = '';
    if(this.party instanceof Party && this.party.winner instanceof Player){
      winnerName = this.party.winner.name;
    }
    return winnerName;
  }
}
