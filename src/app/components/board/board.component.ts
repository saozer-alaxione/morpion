import { NgFor, NgIf } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GAME_AREA_ACTIONS, EMPTY_GAME_AREA, PLAYER_MARKERS, WINNING_POSSIBILITIES } from '../../core/const';

@Component({
  selector: 'app-board',
  standalone: true,
  imports: [NgFor, NgIf],
  templateUrl: './board.component.html',
  styleUrl: './board.component.scss'
})
export class BoardComponent {

  gameboard : Array<Array<string>> = EMPTY_GAME_AREA.map(row => row.slice());

  @Input() set restore(board: Array<Array<string>>){
    this.gameboard = board;
  }

  marker_to_place = PLAYER_MARKERS.PLAYER_X;

  area_state = GAME_AREA_ACTIONS.PLAY;

  @Input() set command(value : GAME_AREA_ACTIONS){
    this.area_state = value;
    if(value == GAME_AREA_ACTIONS.RESET){
      this.clean_area();
    }else if(value == GAME_AREA_ACTIONS.RESTORE_AND_PLAY){
      this.area_state = GAME_AREA_ACTIONS.PLAY;
    }else if(value == GAME_AREA_ACTIONS.INIT_AND_WAIT){
      this.clean_area(true);
    }
  }

  @Output() action = new EventEmitter<{type: GAME_AREA_ACTIONS, marker: string, board: Array<Array<string>>}>();


  constructor(){
  }

  clean_area(lock: boolean | null = false) {
    setTimeout(() => {
      this.marker_to_place = PLAYER_MARKERS.PLAYER_X;
      this.gameboard = EMPTY_GAME_AREA.map(row => row.slice());
      if(lock){
        this.area_state = GAME_AREA_ACTIONS.CLOSE;
        this.action.emit({type: GAME_AREA_ACTIONS.CLOSE, marker: this.marker_to_place, board: this.gameboard});
      }else{
        this.area_state = GAME_AREA_ACTIONS.PLAY;
        this.action.emit({type: GAME_AREA_ACTIONS.PLAY, marker: this.marker_to_place, board: this.gameboard});
      }
    });
  }

  /**
   *
   * @param line line number of the new marker
   * @param col column number of the marker
   */
  place_marker(line: number, col: number){
    if(this.area_state == GAME_AREA_ACTIONS.PLAY && this.cell_is_empty(line, col)){
      // place the current marker
      this.gameboard[line][col] = this.marker_to_place;
      if(this.should_win()){
        this.area_state = GAME_AREA_ACTIONS.WIN;
        this.action.emit({type: GAME_AREA_ACTIONS.WIN, marker: this.marker_to_place, board: this.gameboard});
      }
      this.marker_to_place = this.marker_to_place == PLAYER_MARKERS.PLAYER_O ? PLAYER_MARKERS.PLAYER_X : PLAYER_MARKERS.PLAYER_O;
    }
  }

  /**
   *
   * @param line line number o the cell
   * @param col column number of the cell
   * @returns true | false
   */
  private cell_is_empty(line: number, col: number): boolean{
    return this.gameboard[line][col] != PLAYER_MARKERS.PLAYER_O && this.gameboard[line][col] != PLAYER_MARKERS.PLAYER_X;
  }


  /**
   * let's look if there one possibilitie handled by the same marker
   *
   * @returns true | false
   */
  private should_win(): boolean{
      const lines = WINNING_POSSIBILITIES;
      for (let line of lines) {
        const [mark_1, mark_2, mark_3] = line; // mark_n [x, y]
        if (
          this.gameboard[mark_1[0]][mark_1[1]] && // first mark is placed
          this.gameboard[mark_1[0]][mark_1[1]] === this.gameboard[mark_2[0]][mark_2[1]] && // mark_1 == mark_2
          this.gameboard[mark_1[0]][mark_1[1]] === this.gameboard[mark_3[0]][mark_3[1]] // and mark_2 == mark_3
        ) {
          return true;
        }
      }
      return false;
  }

}
