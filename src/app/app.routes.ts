import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ScoresComponent } from './pages/scores/scores.component';


export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'scores', component: ScoresComponent }
];
